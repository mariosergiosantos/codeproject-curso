## Introdução

Repositório utilizado nas aulas do curso

[Curso Code.Education](http://sites.code.education/laravel-com-angularjs-sv)

## Anotações

- Repository: Ele só vai falar com banco de dados, ele é a nossa abstração do banco de dados.

- Service: A responsabilidade de um serviço tem um escopo mais amplo, ele trata de regras de negócio, por exemplo:

Ao criar um cliente, temos que verificar se ele possui idade maior que 21 anos, caso ele tenha, então ele envia um tweet, caso não, é enviado um e-mail e depois o cliente é criado

- Como decidir se o método irá para o Repositories ou Services?

Se estiver na parte só de consultas, e não transação de informações, é recomendado fazer isso no Repository, e caso haja alguma transação de informação, utilizar no service.