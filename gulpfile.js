var elixir      = require('laravel-elixir'),
    liveReload  = require('gulp-livereload'),
    clean       = require('rimraf'),
    gulp        = require('gulp');

/**
 * Configuração da aplicação
 */
var config = {
    assets_path: './resources/assets',
    build_path:  './public/build'
};

config.bower_path           = config.assets_path    + '/../bower_components';

/**
 * Configuração do javascript
 */
config.build_path_js        = config.build_path     + '/js';
config.build_vendor_path_js = config.build_path_js  + '/vendor';
config.vendor_path_js       = [
    config.bower_path + '/jquery/dist/jquery.min.js',
    config.bower_path + '/bootstrap/dist/js/bootstrap.min.js',
    config.bower_path + '/angular/angular.min.js',
    config.bower_path + '/angular-route/angular-route.min.js',
    config.bower_path + '/angular-resource/angular-resource.min.js',
    config.bower_path + '/angular-animate/angular-animate.min.js',
    config.bower_path + '/angular-messages/angular-messages.min.js',
    config.bower_path + '/angular-bootstrap/ui-bootstrap.min.js',
    config.bower_path + '/angular-strap/dist/modules/navbar.min.js',
    config.bower_path + '/angular-cookies/angular-cookies.min.js',
    config.bower_path + '/query-string/query-string.js',
    config.bower_path + '/angular-oauth2/dist/angular-oauth2.min.js',
];

/**
 * Configuração do css
 */
config.build_path_css        = config.build_path      + '/css';
config.build_vendor_path_css = config.build_path_css  + '/vendor';
config.vendor_path_css       = [
    config.bower_path + '/bootstrap/dist/css/bootstrap.min.css',
    config.bower_path + '/bootstrap/dist/css/bootstrap-theme.min.css',
];

/**
 * Configuração do html
 */
config.build_path_html        = config.build_path      + '/views';

/**
 * Task responsável pela copia do html em
 */
gulp.task('copy-html', function()
{
    gulp.src([
        config.assets_path + '/js/views/**/*.html'
    ])
    .pipe(gulp.dest(config.build_path_html))
    .pipe(liveReload());
});

/**
 * Task responsável pela copia do css em
 * "resources/assets" para "public/build/css" e
 * dos vendors para "public/build/css/vendor"
 */
gulp.task('copy-styles', function()
{
    gulp.src([
        config.assets_path + '/css/**/*.css'
    ])
    .pipe(gulp.dest(config.build_path_css))
    .pipe(liveReload());

    gulp.src(config.vendor_path_css)
    .pipe(gulp.dest(config.build_vendor_path_css))
    .pipe(liveReload());
});

/**
 * Task responsável pela copia do js em
 * "resources/assets" para "public/build/js" e
 * dos vendors para "public/build/js/vendor"
 */
gulp.task('copy-scripts', function()
{
    gulp.src([
        config.assets_path + '/js/**/*.js'
    ])
    .pipe(gulp.dest(config.build_path_js))
    .pipe(liveReload());

    gulp.src(config.vendor_path_js)
    .pipe(gulp.dest(config.build_vendor_path_js))
    .pipe(liveReload());
});

/**
 * Task responsável pela limpesa dos arquivos em
 * "public/build"
 */
gulp.task('clear-build-folder', function()
{
    clean.sync(config.build_path);
});

/**
 * Task default do gulp.
 * Para ser executado em produção
 * Concatena e minifica todos os css's e javascripts
 * da aplicação
 */
gulp.task('default', ['clear-build-folder'], function()
{
    gulp.start('copy-html');
    elixir(function(mix)
    {
        mix.styles(
            config.vendor_path_css.concat([
                config.assets_path + '/css/**/*.css'
            ]
        ), 'public/css/all.css', config.assets_path);

        mix.scripts(
            config.vendor_path_js.concat([
                config.assets_path + '/js/**/*.js'
            ]
        ), 'public/js/all.js', config.assets_path);

        mix.version(['js/all.js', 'css/all.css']);
    });
});

/**
 * Task de desenvolvimento.
 * Para ser executado em ambiente de desenvolvimento
 * Executa as tarefas de copia de css e javascript
 * para o desenvolvimento
 */
gulp.task('watch-dev', ['clear-build-folder'], function()
{
    liveReload.listen();
    gulp.start('copy-styles', 'copy-scripts', 'copy-html');
    gulp.watch(config.assets_path + '/**',
        [
            'copy-styles',
            'copy-scripts',
            'copy-html'
        ]);
});