<?php

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectRepository;
use CodeProject\Validators\ProjectValidator;
use CodeProject\Validators\ProjectFileValidator;
use Prettus\Validator\Exceptions\ValidatorException;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;

class ProjectService
{
    /**
     * @var ProjectRepository
     */
    protected $repository;

    /**
     * @var ProjectValidator
     */
    protected $validator;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Storage
     */
    protected $storage;

    /**
     * @var ProjectFileValidator
     */
    protected $validatorProjectFile;

    /**
     * Constructor of the ProjectService
     */
    public function __construct(ProjectRepository $repository,
                                ProjectValidator $validator,
                                Filesystem $filesystem,
                                Storage $storage,
                                ProjectFileValidator $validatorProjectFile)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->filesystem = $filesystem;
        $this->storage = $storage;
    }

    /**
     * Find all projects
     *
     * @param  integer $id project id
     * @return json
     */
    public function all()
    {
        try
        {
            return $this->repository->with(['owner', 'client', 'members'])->all();
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Find a project by its ID
     *
     * @param  integer $id project id
     * @return json
     */
    public function find($id)
    {
        try
        {
            return $this->repository->with(['owner', 'client', 'members'])->find($id);
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Create a project
     *
     * @param  array  $data project data array
     * @return json
     */
    public function create(array $data)
    {
        try
        {
            $this->validator->with($data)->passesOrFail();
            $result = $this->repository->create($data);

            if(!empty($result->id) && !empty($data['user_id']))
                $this->addMember(['project_id' => $result->id, 'user_id' => $data['user_id']]);

            return $result;
        }
        catch(ValidatorException $e)
        {
            return [
                'error'     => true,
                'message'   => $e->getMessageBag()
            ];
        }
    }

    /**
     * Update the project
     *
     * @param  array  $data project data array
     * @param  integer $id   project id
     * @return json
     */
    public function update(array $data, $id)
    {
        try
        {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        }
        catch(ValidatorException $e)
        {
            return [
                'error'     => true,
                'message'   => $e->getMessageBag()
            ];
        }
    }

    /**
     * Delete the project
     *
     * @param  integer $id   project id
     * @return json
     */
    public function delete($id)
    {
        if($this->repository->delete($id))
            return [
                'error'     => false,
                'message'   => 'Projeto deletado com sucesso'
            ];


        return [
            'error'     => true,
            'message'   => 'Não foi possível deletar o projeto'
        ];
    }

    /**
     * Get all members of a project
     * @param  integer $id projectId
     * @return json
     */
    public function members($id)
    {
        try
        {
            $members = $this->repository->skipPresenter()->find($id)->members;

            if(count($members))
                return $members;

            return [
                'error' => false,
                'message' => 'Não existem membros neste projeto'
            ];
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Add a member to a project
     *
     * @param  integer $id   project id
     * @param  integer $userId   user id
     * @return json
     */
    public function addMember($id, $userId)
    {
        try
        {
            $this->repository->find($id)->members()->attach($userId);
            return ['success' => true];
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Remove a member to a project
     *
     * @param  integer $id   project id
     * @param  integer $userId   user id
     * @return json
     */
    public function removeMember($id, $userId)
    {
        try
        {
            $this->repository->find($id)->members()->detach($userId);
            return ['success' => true];
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Check if a user is member of a project
     *
     * @param  integer $id   project id
     * @param  integer $userId   user id
     * @return json
     */
    public function isMember($id, $userId)
    {
        try
        {
            return $this->repository->find($id)->members()->find($userId) ? ['success' => true, 'isMember' => true] : ['success' => false, 'isMember' => false];
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Inserir arquivo a um projeto
     * @param  array  $data dados enviados
     * @return [type]       [description]
     */
    public function createFile(array $data)
    {
        try
        {

            $this->validatorProjectFile->with($data)->passesOrFail();

            $project = $this->repository->skipPresenter()->find($data['project_id']);
            $projectFile = $project->files()->create($data);

            $this->storage->put($projectFile->id.'.'.$data['extension'], $this->filesystem->get($data['file']));

            return ['success' => true];

        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }


    public function deleteFile($projectId)
    {
        $files = $this->repository->skipPresenter()->find($projectId)->files;

        $deletar = [];
        foreach ($files as $file) {
            $path = $file->id . '.' . $file->extension;

            if($file->delete($file->id))
                $deletar[] = $path;
        }

        $return = $this->storage->delete($deletar);

        if($return)
            return ['error' => false];
        else
            return ['error' => true];

    }


    /**
     * Verifica se o usuário logado é o dono do projecto
     * @param  integer $projectId id do projeto
     * @return boolean            se o usuário é ou não dono do projecto
     */
    public function checkProjectOwner($projectId)
    {
        $userId = \Authorizer::getResourceOwnerId();

        return $this->repository->isOwner($projectId, $userId);
    }

    /**
     * Verifica se o usuário logado é o membro do projecto
     * @param  integer $projectId id do projeto
     * @return boolean
     */
    public function checkProjectMember($projectId)
    {
        $userId = \Authorizer::getResourceOwnerId();

        return $this->repository->hasMember($projectId, $userId);
    }

    /**
     * Verifica se o usuário possui permissão de acesso a um projeto
     * @param  integer $projectId id do projeto
     * @return boolean
     */
    public function checkProjectPermissions($projectId)
    {

        if($this->checkProjectOwner($projectId) || $this->checkProjectMember($projectId))
            return true;

        return false;
    }

}