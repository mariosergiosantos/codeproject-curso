<?php

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectTaskRepository;
use CodeProject\Validators\ProjectTaskValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectTaskService
{
    /**
     * @var ProjectRepository
     */
    protected $repository;

    /**
     * @var ProjectValidator
     */
    protected $validator;

    /**
     * Constructor of the ProjectService
     */
    public function __construct(ProjectTaskRepository $repository,
                                ProjectTaskValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Find all tasks
     * @param  integer $projectId project id
     * @return json
     */
    public function all($projectId)
    {
        try
        {
            return $this->repository->findWhere(['project_id' => $projectId]);
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Find a project note by its ID
     *
     * @param  integer $id project id
     * @return json
     */
    public function find($id, $noteId)
    {
        $note = $this->repository->findWhere(['project_id' => $id, 'id' => $noteId]);

        if($note->count())
            return $note;

        return [
            'error'     => true,
            'message'   => 'Nenhuma task encontrada'
        ];
    }

    /**
     * Create a project
     *
     * @param  array  $data project data array
     * @return json
     */
    public function create(array $data)
    {
        try
        {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        }
        catch(ValidatorException $e)
        {
            return [
                'error'     => true,
                'message'   => $e->getMessageBag()
            ];
        }
    }

    /**
     * Update the project
     *
     * @param  array  $data project data array
     * @param  integer $id   project id
     * @return json
     */
    public function update(array $data, $id)
    {
        try
        {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        }
        catch(ValidatorException $e)
        {
            return [
                'error'     => true,
                'message'   => $e->getMessageBag()
            ];
        }
    }

    /**
     * Delete the project task
     *
     * @param  integer $id   project id
     * @return json
     */
    public function delete($id)
    {
        if($this->repository->delete($id))
            return [
                'error'     => false,
                'message'   => 'Task deletada com sucesso'
            ];


        return [
            'error'     => true,
            'message'   => 'Não foi possível deletar a task'
        ];
    }

}