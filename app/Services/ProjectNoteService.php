<?php

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectNoteRepository;
use CodeProject\Validators\ProjectNoteValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectNoteService
{
    /**
     * @var ProjectRepository
     */
    protected $repository;

    /**
     * @var ProjectValidator
     */
    protected $validator;

    /**
     * Constructor of the ProjectService
     */
    public function __construct(ProjectNoteRepository $repository,
                                ProjectNoteValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Get all project notes
     * @param  integer $projectId project id
     * @return json
     */
    public function all($projectId)
    {
        try
        {
            return $this->repository->findWhere(['project_id' => $projectId]);
        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Find a project note by its ID
     *
     * @param  integer $id project id
     * @return json
     */
    public function find($id, $noteId)
    {
        $note = $this->repository->findWhere(['project_id' => $id, 'id' => $noteId]);

        if($note->count())
            return $note;

        return [
            'error'     => true,
            'message'   => 'Nenhuma nota encontrada'
        ];
    }

    /**
     * Create a project
     *
     * @param  array  $data project data array
     * @return json
     */
    public function create(array $data)
    {
        try
        {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        }
        catch(ValidatorException $e)
        {
            return [
                'error'     => true,
                'message'   => $e->getMessageBag()
            ];
        }
    }

    /**
     * Update the project
     *
     * @param  array  $data project data array
     * @param  integer $id   project id
     * @return json
     */
    public function update(array $data, $id)
    {
        try
        {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        }
        catch(ValidatorException $e)
        {
            return [
                'error'     => true,
                'message'   => $e->getMessageBag()
            ];
        }
    }

    /**
     * Delete the project note
     *
     * @param  integer $id   project id
     * @return json
     */
    public function delete($id)
    {
        try
        {
            $this->repository->delete($id);

            return ['success' => true];

        }
        catch (\Exception $e)
        {
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }
    }

}