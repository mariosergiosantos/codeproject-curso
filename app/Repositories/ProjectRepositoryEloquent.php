<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeProject\Entities\Project;
use CodeProject\Presenters\ProjectPresenter;

/**
 * Class ProjectRepositoryEloquent
 * @package namespace CodeProject\Repositories;
 */
class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria( app(RequestCriteria::class) );
    }

    /**
     * Verifica se o usuário é dono do projecto
     * @param  integer  $projectId id do projecto
     * @param  integer  $userId    id do usuário
     * @return boolean
     */
    public function isOwner($projectId, $userId)
    {
        if(count($this->findWhere(['id' => $projectId, 'owner_id' => $userId])))
            return true;

        return false;
    }

    /**
     * Verifica se o usuário é membro do projecto
     * @param  integer  $projectId id do projeto
     * @param  integer  $memberId  id do membro do projeto
     * @return boolean
     */
    public function hasMember($projectId, $memberId)
    {
        $project = $this->find($projectId);

        foreach($project->members as $member)
        {
            if ($member->id == $memberId)
                return true;
        }

        return false;
    }

    /**
     * Retorna o presenter do Repository
     * @return ProjectPresenter
     */
    public function presenter()
    {
        return ProjectPresenter::class;
    }
}