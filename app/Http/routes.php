<?php

Route::get('/', function () {
    return view('app');
});

Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});


Route::group(['middleware' => 'oauth'], function()
{

    Route::resource('client', 'ClientController', ['except' => ['create', 'edit']]);

    Route::resource('project', 'ProjectController', ['except' => 'create', 'edit']);

    Route::group(['prefix' => 'project'], function()
    {

        /**
         * Project Notes
         */

        Route::get('{id}/note', 'ProjectNoteController@index');
        Route::post('{id}/note', 'ProjectNoteController@store');
        Route::get('{id}/note/{noteId}', 'ProjectNoteController@show');
        Route::put('{id}/note/{noteId}', 'ProjectNoteController@update');
        Route::delete('{id}/note/{noteId}', 'ProjectNoteController@destroy');

        /**
         * Project Task Routes
         */

        Route::get('{id}/task', 'ProjectTaskController@index');
        Route::post('{id}/task', 'ProjectTaskController@store');
        Route::get('{id}/task/{taskId}', 'ProjectTaskController@show');
        Route::put('{id}/task/{taskId}', 'ProjectTaskController@update');
        Route::delete('{id}/task/{taskId}', 'ProjectTaskController@destroy');

        /**
         * Project Members Routes
         */
        Route::get('{id}/members', 'ProjectController@members');
        Route::post('{id}/member', 'ProjectController@addMember');
        Route::delete('{id}/member/{userId}', 'ProjectController@removeMember');
        Route::get('{id}/member/{userId}', 'ProjectController@isMember');


        /**
         * Project Files
         */
        Route::post('{id}/file', 'ProjectFileController@store');
        Route::delete('{id}/file', 'ProjectFileController@destroy');
    });

});